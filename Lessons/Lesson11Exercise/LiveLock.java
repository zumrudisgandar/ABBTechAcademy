public class LiveLock {
    public static void main(String[] args) {
        Car tesla = new Car("Tesla", true);
        Car bmw = new Car("BMW", true);

        Thread teslaThread = new Thread(() -> tesla.pass(bmw));
        teslaThread.start();

        Thread bobThread = new Thread(() -> bmw.pass(tesla));
        bobThread.start();
    }
}
