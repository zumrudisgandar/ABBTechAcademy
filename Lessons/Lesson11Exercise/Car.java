public class Car {
    private String brand;
    private boolean polite;

    public void pass(Car car) {
        while(car.isPolite()){
            System.out.println(brand + ": After you, " + car.getBrand());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (car.isPolite()) {
                continue;
            }

            System.out.println(brand + ": Thank you!");
            break;
        }
    }
    public Car(String brand, boolean polite) {
        this.brand = brand;
        this.polite = polite;
    }

    public String getBrand() {
        return brand;
    }

    public boolean isPolite() {
        return polite;
    }

}
