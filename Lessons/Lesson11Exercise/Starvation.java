import java.util.concurrent.locks.ReentrantLock;

public class Starvation {
    static class Runner implements Runnable {
        private ReentrantLock lock;
        private String name;

        public Runner(ReentrantLock lock, String name) {
            this.lock = lock;
            this.name = name;
        }

        public void run(){
            while(true) {
                lock.lock();
                try {
                    System.out.println(name + " is running");
                    Thread.sleep(100);
                } catch(InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        }
    }

    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        Runner highPriorityRunner = new Runner(lock, "High Priority Runner");
        Runner lowPriorityRunner = new Runner(lock, "Low Priority Runner");

        Thread highPriorityThread = new Thread(highPriorityRunner);
        Thread lowPriorityThread = new Thread(lowPriorityRunner);

        highPriorityThread.setPriority(Thread.MAX_PRIORITY);
        lowPriorityThread.setPriority(Thread.MIN_PRIORITY);

        highPriorityThread.start();
        lowPriorityThread.start();
        highPriorityThread.interrupt();
    }
}
